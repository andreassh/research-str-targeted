# Split one reference fasta file into multiple contigs files
# Use: python3 splitfasta.py /location/of/hg38.fa output_chroms/folder/

import os
import sys

filename = sys.argv[1]
output_folder = sys.argv[2]
fasta_file = open(filename)

print("Reference file: " + filename)

opened = False # Assume outfile is not open

for line_ref in fasta_file:
	if line_ref[0] == ">": # If line begins with ">"
		if(opened): 
			outfile.close() # Will close the outfile if it is open (see below and follow loop)
		opened = True # Set opened to True to represent an opened outfile
		contig_name = line_ref[0:].split(" ")[0].split("\t")[0].lstrip(">").strip() # Extract contig name: remove ">" and spaces and tabs
		print("Contig: " + contig_name + " / Name of outfile: " + str(sys.argv[1].split('/')[-1].split(".")[0]) + "_" + str(contig_name) + ".fa") 
		outfile = open(str(output_folder) + "/" + str(sys.argv[1].split('/')[-1].split(".")[0]) + "_" + str(contig_name) + ".fa", 'w')
	outfile.write(line_ref)

outfile.close()

print ("Finished")
