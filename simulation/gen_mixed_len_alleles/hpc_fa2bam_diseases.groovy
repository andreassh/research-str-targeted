load "../hpc_config.groovy"

def get_filename(path) {
	x = path.split("/")[-1]
	return(x)
}
def get_fileinfo(filename) {
	return(filename.split('/')[-1].split('\\.'))
}

// Simulate reads based on the simulated reference files, make alleles when simulating HiSeqX PCR free (150bp) reads with fragment length of 450 bp
sim_reads = {
	def fa_filename = get_filename(input.fa)
	preserve("*.fq") {
		from("*.a1.fa") {
			produce(fa_filename.prefix.prefix + ".a1.R1.fq", fa_filename.prefix.prefix + ".a1.R2.fq") {
				exec """
					art_illumina \
					-q \
					-ss HSXn \
					-p \
					-na \
					-l 150 \
					-f 25 \
					-m 450 \
					-s 50 \
					-i $input.fa \
					-o $output1.prefix.prefix".R"
				""", "superquick"
			}
		}

		from("*.a2.fa") {
			produce(fa_filename.prefix.prefix + ".a2.R1.fq", fa_filename.prefix.prefix + ".a2.R2.fq") {
				exec """
					art_illumina \
					-q \
					-ss HSXn \
					-p \
					-na \
					-l 150 \
					-f 25 \
					-m 450 \
					-s 50 \
					-i $input.fa \
					-o $output2.prefix.prefix".R"
				""", "superquick"
			}
		}
	}
}

// Merge reads of two alleles together
sim_concat_fq = {
	def fileinfo = get_fileinfo(input)
	def samplename = fileinfo[0]
	produce(samplename + '.R1.fq', samplename + '.R2.fq') {
		multi "cat $samplename'.a1.R1.fq' $samplename'.a2.R1.fq' > $samplename'.R1.fq'",
			  "cat $samplename'.a1.R2.fq' $samplename'.a2.R2.fq' > $samplename'.R2.fq'"
	}
}

// Align reads on the reference genome
sim_align = {
	def fileinfo = get_fileinfo(inputs)
	def samplename = fileinfo[0]
	produce(samplename + '.bam') {
		exec """
				bwa mem -M -t $BWA_THREADS \
				-R "@RG\\tID:${samplename}\\tPL:$PLATFORM\\tPU:1\\tLB:${samplename}\\tSM:${samplename}" \
				$REF_FILE $inputs | \
				samtools view -bSuh - | samtools sort -o $output.bam -
		""", "superquick"
	}
}

// Create index for BAM
sim_indxbam = {
	exec """
		samtools index $input.bam
	""", "superquick"
	forward input.bam
}

run {
	"%.*a*" * [
		 "%.fa" * [ sim_reads ]  +
		 sim_concat_fq +
		 sim_align + 
		 sim_indxbam
	]
}