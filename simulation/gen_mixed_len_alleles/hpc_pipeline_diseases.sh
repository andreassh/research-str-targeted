#!/usr/bin/env bash

if [[ -z $1 ]]; then
	echo "A disorder was not supplied"
else
	echo "Running pipeline for $1"

	mkdir -p out/fq/$1 out/bam/$1 out/eh/$1

	cd out/fq/$1/
	bpipe run ../../../hpc_fa2bam_diseases.groovy ../../fa/$1/*.fa
	mv *.bam* ../../bam/$1/

	cd ../../eh/$1/
	bpipe run ../../../../hpc_bam2expansionhunter.groovy ../../bam/$1/*.bam

	echo "Pipeline finished for $1"
fi


