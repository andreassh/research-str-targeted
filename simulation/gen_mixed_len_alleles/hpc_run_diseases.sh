#!/usr/bin/env bash

output_folder=out/

reference_fasta=../../reference/hg38/hg38.fa
reference_chroms_folder=../../reference/hg38/chroms/
reference_genome=hg38
disorders_list=../../reference/strs.loci


echo "Do you wish to delete all previous output files?"
select del in "Yes" "No"; do
	case $del in
		Yes ) rm -rf ${output_folder}*; break;;
		No ) break;;
	esac
done

echo "Do you wish to run simulation on all files now?"
select sim in "Yes" "No"; do
	case $sim in
		Yes ) 
			module load bpipe

			python3 ../simulate_dis_samples.py --reference ${reference_fasta} --reference_folder ${reference_chroms_folder} --genome ${reference_genome} --disorders ${disorders_list} --output_folder ${output_folder}

			while read -a line;
			do
				./hpc_pipeline_diseases.sh ${line[0]} ${output_folder}

				python3 ../extract_eh_results.py --disorder ${line[0]} --gene ${line[1]} --input_eh_folder ${output_folder}eh/ --output ${output_folder}res_extracted/${line[0]}.results --simulated_samples disease
			done < ${output_folder}loci.sequences

			sed -e '3,${/^disorder/d' -e '}' ${output_folder}res_extracted/*.results > ${output_folder}res_extracted/combined.allresults;
			break;;

		No ) exit;;
	esac
done

echo "Reached to the end of the list!"
