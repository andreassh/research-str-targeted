load "hpc_config.groovy"

// Call STRs with ExpansionHunter
sim_eh = {
	transform('.vcf', '.json') {
		exec """
			$EXPANSIONHUNTER \
				--reads $input.bam \
				--reference $REF_FILE \
				--variant-catalog $EH_VARIANT_CATALOG \
				--output-prefix $output.prefix
		""", "superquick"
	}
}

run {
	~'(.*).bam' * [
		sim_eh
	]
}
