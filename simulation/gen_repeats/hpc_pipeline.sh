#!/usr/bin/env bash

# Input disease is $disease and output folder is $output_folder, motif is $3 and repeats max bp is $4, zygosity is $5
disease=$1
output_folder=$2
motif=$3
min_allele_len_bp=$4
max_allele_len_bp=$5
zygosity=$6

motiflen=${#motif} # Length of motif in base pairs
minsamples=$(($min_allele_len_bp / $motiflen)) # Repeats min bp divided by motif length
maxsamples=$(($max_allele_len_bp / $motiflen)) # Repeats max bp divided by motif length

if [[ -z $disease ]]; then
	echo "A disorder was not supplied"
else
	echo "Running pipeline for $disease"

	# Create folders
	mkdir -p $output_folder/fq/$disease $output_folder/bam/$disease $output_folder/eh/$disease

	# Simulate reads
	cd $output_folder/fq/$disease/

	for i in $(seq $minsamples 25 $maxsamples) # From min to max in 25 steps
	do
		samples=''
		plus=$(($i + 24))
		for j in $(seq $i 1 $plus)
		do
			if [[ $j -lt 100 ]]
			then
				zeros="0${zeros}"
				zeros="${zeros: -1}"
			else
				zeros=""
			fi

			if [[ $j -le $maxsamples ]]
			then
				samples+="../../fa/$disease/${zeros}${j}rep*.fa "
			fi
		done
		
		bpipe run ../../../hpc_fa2bam_${zygosity}.groovy ${samples}
		bpipe cleanup -y
	done

	# Move BAMs to another folder
	mv *.bam* ../../bam/$disease/

	# ExpansionHunter
	cd ../../eh/$disease/
	bpipe run ../../../hpc_bam2expansionhunter.groovy ../../bam/$disease/*.bam

	echo "Pipeline finished for $disease"
fi
