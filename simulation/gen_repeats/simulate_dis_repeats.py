################################################################################################################################################################################################################################################
# Python script to create new reference alleles (fasta files) which has simulated STR expansions in it                                                                                                                                         #
# Example usage: python3 simulate_dis_repeats.py --reference ../reference/hg38/hg38.fa --reference_folder ../reference/hg38/chroms/ --genome hg38 --disorders ../reference/strs.loci --zygosity het --min 60 --max 2100 --output_folder out/   #
################################################################################################################################################################################################################################################

import random
import csv
import re
from pathlib import Path
from argparse import (ArgumentParser, FileType)
from Bio import SeqIO
from Bio.Seq import Seq

# Simulation parameters
flanking_seq_len = 2000 # How many base pairs before and after the STR sequence will be taken
normal_allele1_len_bp = 60

healthy_rep_min = 2
healthy_rep_max = 10
repeats_normal_min = 2 # If normal range of repeats is specified to be less than X value, then take this value as minimum

# Arguments from command line
def parse_args():
	parser = ArgumentParser(description='Generate samples with tandem repeat expansions')
	parser.add_argument('--reference', type=str, required=True, help='Input reference file.')
	parser.add_argument('--reference_folder', type=str, required=True, help='Folder where reference file choromosomes are.')
	parser.add_argument('--genome', type=str, required=True, help='Reference file genome.')
	parser.add_argument('--disorders', type=str, required=True, help='File which includes information about STRs loci and coordinates of loci')
	parser.add_argument('--output_folder', type=str, required=True, help='Folder for output files')
	parser.add_argument('--only_loci', type=str, required=False, help='Specify if you only want to write the loci file')
	parser.add_argument('--min', type=str, required=True, help='Minimum length of repeats in base pairs')
	parser.add_argument('--max', type=str, required=True, help='Maximum length of repeats in base pairs')
	parser.add_argument('--zygosity', type=str, required=True, choices=['hom', 'het'], help='Simulate homozygous or heterozygous repeats?')
	
	return parser.parse_args()


# Generate random number for repeats based on the min and max values
def gen_rand_rep_no(rep_min, rep_max):
	return random.randrange(rep_min, rep_max) if rep_min != rep_max else rep_min


# Replace all N values with randomly chosen nucleotide
def replace_nucleotides(sequence):

	# All symbols that can be used to replace a placeholder nucleotide for another one
	nucleotides = {
		"R": "GA",
		"Y": "CT",
		"K": "GT",
		"M": "AC",
		"S": "GC",
		"W": "AT",
		"B": "GTC",
		"D": "GAT",
		"H": "ACT",
		"V": "GCA",
		"N": "AGCT"
	}

	new_sequence = ""

	# Go through each nucleotide and replace the ones than can be with a randomly chosen nucleotide from the list above
	for base in sequence:
		new_sequence += base.replace(base, random.choice(nucleotides[base])) if base not in "AGCT" else base

	return new_sequence


# Generate allele lengths
def make_alleles(disorder, path_repeats):
	if disorder["motif"][1:2] == "!":
		repeat_type = disorder["motif"][0:1]
		motif = disorder["motif"][2:]
	else:
		repeat_type = ""
		motif = disorder["motif"]

	motiflen = int(len(disorder["motif_pathogenic"]))

	complex_motif = True if repeat_type == "n" or repeat_type == "r" else False

	disorder_recessive = True if "R" in disorder["inheritance"] else False # If "R" because R is in AR and XLR only
	disorder_Xlinked = True if "XL" in disorder["inheritance"] else False # For calculation whether locus is heterozygous or homozygous

	# If it is not inherited then create alleles as they would be dominant
	if disorder["inheritance"] == "NA":
		disorder_recessive = False
	
	# Set min and max of healthy motif for nested repeats
	rep_min = healthy_rep_min
	rep_max = healthy_rep_max

	# Generate repeat lengths for alleles
	path_rep_len_a1 = int(normal_allele1_len_bp / motiflen) if req_zygosity == 'het' else path_repeats # If homozygous then both alleles are the same, if heterozygous then create a fixed length allele
	path_rep_len_a2 = int(path_repeats) # Fixed length for pathogenic allele 2
	path_rep_len = [path_rep_len_a1, path_rep_len_a2]

	homozygous = False

	if complex_motif:
		if repeat_type == "n":
			alleles_len = [random.randrange(rep_min, rep_max), random.randrange(rep_min, rep_max)]

		elif repeat_type == "r":
			alleles_len = [0, 0]

		alleles_len.sort()

		seq_allele1 = generate_repeat_sequence("complex-repeats", motif, repeat_type, alleles_len[0], path_rep_len[0] if req_zygosity == 'hom' else 0)
		seq_allele2 = generate_repeat_sequence("complex-repeats", motif, repeat_type, alleles_len[1], path_rep_len[1])
	else:
		alleles_len = [path_rep_len[0], path_rep_len[1]]
		seq_allele1 = generate_repeat_sequence("pure-repeats", motif, repeat_type, alleles_len[0], 0)
		seq_allele2 = generate_repeat_sequence("pure-repeats", motif, repeat_type, alleles_len[1], 0)

	if complex_motif:
		alleles_len.append(path_rep_len[0]) # Add number of repeats of pathogenic repeats as a third and fourth element
		alleles_len.append(path_rep_len[1])

	write_stats_file(disorder["name"], path_repeats, alleles_len, [seq_allele1, seq_allele2])

	return [seq_allele1, seq_allele2]


# Generate allele sequences
def generate_repeat_sequence(algorithm, motif_in, repeat_type, rep_len, path_rep_len):
	# Pure repeats algorithm creates alleles with the same sequence repeated n times (e.g. HD, SBMA, etc.)
	if algorithm == "pure-repeats":
		allele_seq = motif_in * rep_len

	# Complex repeats algorithm, which will include replaced repeats or nested repeats generation
	elif algorithm == "complex-repeats":
		# Generate repeat sequence based on the repeat length obtained and use a proper algoritm for complex repeats
		motifs = re.findall(r'\(([A|T|G|C|N]*)\)(exp|\d*)|\[([A|T|G|C|N]*)\](exp|\d*)', motif_in) # Elements 0 and 1 in a list are normal motifs and elements 2 and 3 are pathogenic motifs
		normal_add_rep_random = 1
		path_add_rep_random = 1
		allele_seq = ''

		# Nested repeats - creates alleles where the pathogenic motif is inserted inside of non-pathogenic repeats, e.g. SCA 31, FAME 1, 2, 6 and 7. Formulas can be used (e.g. [AAATG]exp(AAAAT)exp for FAME 2)
		if repeat_type == "n":
			for motif in motifs: # Go the repeat formula through one-by-one
				if motif[0]: # If the motif is the normal one
					allele_repeats_count = rep_len
		
					if motif[1].isdigit(): # Check whether number of expansions is defined
						allele_repeats_count = rep_len - int(motif[1]) if int(motif[1]) < rep_len else 0 # If number of expansions defined is larger than the normal allele length, then mark leftover repeats as 0
						allele_seq += motif[0] * int(motif[1]) if motif[1].isdigit() else motif[0] * allele_repeats_count

					if motif[1] == "exp": # If the number of expansions is not defined, then create expansions in random length
						if allele_repeats_count <= 0: # If there are no leftover repeats...
							repeats_for_exp = random.randrange(2, 10) # Generate random number between 2 and 10
						else:
							repeats_for_exp = allele_repeats_count if allele_repeats_count >= 2 else 2 # If leftover is less than 2, then use 2

						allele_seq += motif[0] * int(motif[1]) if motif[1].isdigit() else motif[0] * repeats_for_exp

				elif motif[2]: # If the motif is the pathogenic one
					if path_rep_len > 0:
						allele_seq += motif[2] * int(motif[3]) if motif[3].isdigit() else motif[2] * path_rep_len

		# Replaced repeats - where a normal motif is replaced with a pathogenic motif (e.g. CANVAS where (AAAAG)11 is replaced with (AAGGG)exp which is linked to the disorder)
		elif repeat_type == "r":
			for motif in motifs:
				if path_rep_len == 0:
					if motif[0]:
						allele_repeats_count = rep_len
			
						if motif[1].isdigit():
							allele_seq += motif[0] * int(motif[1]) if motif[1].isdigit() else motif[0] * allele_repeats_count

						if motif[1] == "exp":
							repeats_for_exp = allele_repeats_count if allele_repeats_count >= 2 else 2

							allele_seq += motif[0] * int(motif[1]) if motif[1].isdigit() else motif[0] * repeats_for_exp
					else:
						continue
				else:
					if motif[2]: # If the motif is the pathogenic one
						allele_seq += motif[2] * int(motif[3]) if motif[3].isdigit() else motif[2] * path_rep_len

	allele_seq = replace_nucleotides(allele_seq) # If there are any N bases, then replace them with one of the 4 nucleotides
	return allele_seq


# Find the flanking sequences of a repeat locus
def get_flanking_sequences(chrom, str_start, str_end):
#	for record in SeqIO.parse(fasta_ref_file, 'fasta'):
	for record in SeqIO.parse(open(file_chroms_folder + genome + "_" + chrom + ".fa", 'r'), 'fasta'):
		if (record.id == chrom):
			flank_start = str_start - flanking_seq_len # Start coordinates for the whole locus
			flank_end = str_end + flanking_seq_len # End coordinates for the whole locus

			flank_start_seq = record.seq[flank_start:str_start] # Sequence before the STR
			flank_end_seq = record.seq[str_end:flank_end] # Sequence after the STR

			return [flank_start_seq, flank_end_seq]


# Get new coordinates of a whole locus after counting in the expansion
def get_new_coordinates(chrom, str_start, sequence):
	new_seq_len = len(sequence)
	locus_start = str_start - flanking_seq_len
	locus_end = locus_start + new_seq_len
	coord_locus = str(chrom) + ":" + str(locus_start) + "-" + str(locus_end) # Coordinates of the whole locus

	return coord_locus


# Write fasta files with a new sequence
def write_fasta(disorder, file_id, coordinates, sequence):
	output_disorder_folder = output_fasta_folder + disorder["name"] + "/"

	f = open(output_disorder_folder + file_id + ".fa", "w")
	f.write(">" + coordinates + "\n")
	f.write(str(sequence).upper())
	f.close()


# Write a stats file that contains information about the disorder and newly created alleles (such as alelles' length and sequence)
def write_stats_file(disorder_name, path_repeats, alleles_len, alleles_seq):
	output_disorder_folder = output_fasta_folder + disorder_name + "/"
	Path(output_disorder_folder).mkdir(parents=True, exist_ok=True) # Create a folder if doesn't exist yet

	with open(output_disorder_folder + "alleles.stat", "a") as outfile:
		outfile.write(str(path_repeats) + "\t")
		if len(alleles_len) == 4: # If there are 4 elements then it means there is additional information (number of repeats of new pathogenic motifs)
			outfile.write(str(alleles_len[0]) + "(" + str(alleles_len[2]) + "), " + str(alleles_len[1]) + "(" + str(alleles_len[3]) + ")\n")
		else:
			outfile.write(", ".join(str(item) for item in alleles_len) + "\n")


# Make a file that includes the flanking sequences
def write_flank_seq_file(disorder, flank_seq):
	with open(output_folder + "loci.sequences", "a") as outfile:
		outfile.write(
			str(disorder["name"]) + "\t" + 
			str(disorder["gene"]) + "\t" + 
			str(disorder["coord_chrom"]) + "\t" + 
			str(disorder["coord_str_start"]) + "\t" + 
			str(disorder["motif_pathogenic"]) + "\t" + 
			str(flank_seq[0].upper()) + "\t" + 
			str(flank_seq[1].upper()) + "\n")


def main():
	global file_reference, file_chroms_folder, genome, file_disorders, output_folder, output_fasta_folder, range_min_bp, range_max_bp, req_zygosity
	args = parse_args()
	file_reference = args.reference
	file_chroms_folder = args.reference_folder
	genome = args.genome
	only_make_loci_file = args.only_loci
	file_disorders = args.disorders
	output_folder = args.output_folder
	req_zygosity = args.zygosity
	output_fasta_folder = args.output_folder + "fa/"
	range_min_bp = int(args.min)
	range_max_bp = int(args.max) + 1

	# Open the disords list file and import the data into a dictonary
	disorders = []
	with open(file_disorders, mode="r") as infile:
		reader = csv.reader(infile, delimiter="\t")
		headers = next(reader, None)

		for col in reader:
			coordinates = re.split(':|-',col[8])
			repeats_normal = col[6]

			if "<=" in repeats_normal: # If the minimum value is not specified, then specify one
				repeats_normal_list = [repeats_normal_min, int(repeats_normal.split("<=")[1])]
			else:
				repeats_normal_list = list(map(int, col[6].split("-"))) # To map and then to list again to get list of integers instead of strings

				if len(repeats_normal_list) == 1: # If only one value is for range, then min and max is the same
					repeats_normal_list.append(int(repeats_normal_list[0]))

			risk_cutoff = repeats_normal_list[1] if col[7] == "NA" else col[7] # If risk cut-off is not specified then take the maximum value from the normal range

			disorder = {
				"name": col[0],
				"inheritance": col[1],
				"gene": col[2],
				"location": col[3],
				"motif_pathogenic": col[4],
				"motif": col[5], # Use motif's formula here
				"repeats_normal": repeats_normal_list,
				"risk_cutoff": int(col[7]),
				"coord_chrom": coordinates[0],
				"coord_str_start": int(coordinates[1]),
				"coord_str_end": int(coordinates[2])
			}
			disorders.append(disorder)


	# Go through each disoder in the disorder file list
	for disorder in disorders:
		# Print out what we are doing
		print("Generating alleles for: " + disorder["name"])

		flanking_sequences = get_flanking_sequences(disorder["coord_chrom"], disorder["coord_str_start"], disorder["coord_str_end"])

		# Write a sequences file for each locus
		write_flank_seq_file(disorder, flanking_sequences)

		if only_make_loci_file: # If user only wants to get the loci file, then don't create allele files
			continue

		Path(output_fasta_folder).mkdir(parents=True, exist_ok=True) # Create a folder if doesn't exist yet

		motiflen = int(len(disorder["motif_pathogenic"]))
		range_min_repeats = int(range_min_bp / motiflen) if int(range_min_bp / motiflen) >= 2 else 2
		range_max_repeats = int(range_max_bp / motiflen)+1 if int(range_max_bp / motiflen) > range_min_repeats else range_min_repeats+1 # Add +1 to get the repeats until the max bp

		for i in range(range_min_repeats, range_max_repeats, 1):
			i_str = str(i)

			if len(i_str) == 1:
				i_filename = "00" + i_str
			elif len(i_str) == 2:
				i_filename = "0" + i_str
			else:
				i_filename = i_str

			new_alleles = make_alleles(disorder, i)

			for i, allele in enumerate(new_alleles):
				allele_nr = i + 1

				new_whole_seq = flanking_sequences[0] + allele + flanking_sequences[1] # Stitch together flanking ends and new allele
				locus_coordinates = get_new_coordinates(disorder["coord_chrom"], disorder["coord_str_start"], new_whole_seq) # Get the new locus coordinates (the end changes considering how many bp will be added)

				file_id = i_filename + "rep.a" + str(allele_nr) # Use disorder name, sample type and allele number in file names
				write_fasta(disorder, file_id, locus_coordinates, new_whole_seq) # Write output file


if __name__ == "__main__":
	main()
