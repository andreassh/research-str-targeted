#!/usr/bin/env bash

output_folder=../out_hom2100/ # Specify the whole system path
zygosity=hom # Use 'het' to simulate heterozygous samples and 'hom' to simulate homozygous ones

repeats_min_bp=60 # In base pairs. NB! Minimum number of repeats is always 2 even when specified less base pairs in here
repeats_max_bp=2100 # In base pairs

reference_fasta=../../reference/hg38/hg38.fa
reference_chroms_folder=../../reference/hg38/chroms/
reference_genome=hg38
disorders_list=../../reference/strs.loci

echo "Do you wish to run simulation on all files now?"
select sim in "Yes" "No"; do
	case $sim in
		Yes ) 
			module load bpipe

			mkdir -p ${output_folder}

			python simulate_dis_repeats.py --reference ${reference_fasta} --reference_folder ${reference_chroms_folder} --genome ${reference_genome} --disorders ${disorders_list} --zygosity ${zygosity} --min ${repeats_min_bp} --max ${repeats_max_bp} --output_folder ${output_folder}

			while read -a line;
			do
				./hpc_pipeline.sh ${line[0]} ${output_folder} ${line[4]} ${repeats_min_bp} ${repeats_max_bp} ${zygosity} # Send to pipeline values: disorder, output folder, motif, repeats max bp 
#				./hpc_pipeline_eh.sh ${line[0]} ${output_folder}

				python ../extract_eh_results.py --disorder ${line[0]} --gene ${line[1]} --input_eh_folder ${output_folder}eh/ --output ${output_folder}res_extracted/${line[0]}.results --simulated_samples repeats
			done < ${output_folder}loci.sequences;

			sed -e '3,${/^disorder/d' -e '}' ${output_folder}res_extracted/*.results > ${output_folder}res_extracted/combined.allresults;
			break;;

		No ) exit;;
	esac
done

echo "Reached to the end of the list!"

