load "hpc_config.groovy"

def get_filename(path) {
	return(path.split("/")[-1])
}

def get_fileinfo(filename) {
	return(filename.split('/')[-1].split('\\.'))
}

// Simulate reads
@intermediate
sim_reads_a1 = {
	def filename = get_filename(input.fa)
	int pcr = filename.split('\\.')[1].substring(0,3).toInteger()
	int coverage = filename.split('\\.')[2].substring(0,3).toInteger()

	from("*.a1.fa") {
		produce(filename.prefix.prefix + ".a1.R1.fq", filename.prefix.prefix + ".a1.R2.fq") {
			exec """
				art_illumina \
				-q \
				-ss HSXn \
				-p \
				-na \
				-f ${coverage} \
				-l 150 \
				-m 450 \
				-s 50 \
				-i $input.fa \
				-o $output.prefix.prefix".R"
			""", "superquick"
		}
	}
}

@intermediate
sim_reads_a2 = {
	def filename = get_filename(input.fa)
	int pcr = filename.split('\\.')[1].substring(0,3).toInteger()
	int coverage = filename.split('\\.')[2].substring(0,3).toInteger()

	from("*.a2.fa") {
		produce(filename.prefix.prefix + ".a2.R1.fq", filename.prefix.prefix + ".a2.R2.fq") {
			exec """
				art_illumina \
				-q \
				-ss HSXn \
				-p \
				-na \
				-f ${coverage} \
				-l 150 \
				-m 450 \
				-s 50 \
				-i $input.fa \
				-o $output.prefix.prefix".R"
			""", "superquick"
		}
	}
}

// Merge reads of two alleles together
@intermediate
sim_concat_fq = {
	def filename = get_filename(input.fa)
	def samplename = filename.split('\\.')[0]

	produce(samplename + '.R1.fq', samplename + '.R2.fq') {
		multi "cat ${samplename}.*.a1.R1.fq ${samplename}.*.a2.R1.fq > $output1",
	          "cat ${samplename}.*.a1.R2.fq ${samplename}.*.a2.R2.fq > $output2"
	}
}

// Align reads on the reference genome
@preserve
sim_align = {
	def fileinfo = get_fileinfo(inputs)
	def samplename = fileinfo[0]

	produce(samplename + '.bam') {
		exec """
			bwa mem -M -t $BWA_THREADS \
			-R "@RG\\tID:${samplename}\\tPL:$PLATFORM\\tPU:1\\tLB:${samplename}\\tSM:${samplename}" \
			$REF_FILE $inputs | \
			samtools view -bSuh - | samtools sort -o $output.bam -
		""", "quick"
	}
}

// Create index for BAM
@preserve
sim_indxbam = {
	def fileinfo = get_fileinfo(inputs)
	def samplename = fileinfo[0]

	produce(samplename + '.bam.bai') {
		exec """
			samtools index $input.bam
		""", "superquick"
	}
}

// Run it
run {
	"%rep.*.fa" * [
		"%pcr.*.fa" * [
			 "%.a1.fa" * [ sim_reads_a1 ], 
			 "%.a2.fa" * [ sim_reads_a2 ]
		] +
			sim_concat_fq +
			sim_align + 
			sim_indxbam 
	]
}
