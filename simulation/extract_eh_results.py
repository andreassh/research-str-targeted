# Python script to exctract genotyped allele lengths out from ExpansionHunter VCF files
# Use: python3 extract_eh_results.py --disorder HD --gene HTT --output HD.results --input_eh_folder out/eh/ --simulated_samples repeats

import csv
import re
import sys
import os
from argparse import (ArgumentParser, FileType)
from pathlib import Path

def parse_args():
	parser = ArgumentParser(description='Extracts genotypes from ExpansionHunter VCF files')
	parser.add_argument('--disorder', type=str, required=True, help='Disorder')
	parser.add_argument('--gene', type=str, required=True, help='Gene')
	parser.add_argument('--output', type=str, required=True, help='Output file')
	parser.add_argument('--input_eh_folder', type=str, required=True, help='Input folder where ExpansionHunter VCF files are located at')
	parser.add_argument('--simulated_samples', type=str, required=True, help='Simulated samples type', choices=['disease', 'repeats'])
	
	return parser.parse_args()


# Extract results from ExpansionHunter VCFs
def get_call_eh(call):
	has_results = False if call[9][0] == "." else True
	if has_results:
		STR_coverage_spanning = call[9].split(":")[4].split("/")
		STR_coverage_flanking = call[9].split(":")[5].split("/")
		STR_coverage_inrepeat = call[9].split(":")[6].split("/")
		CI = call[9].split(":")[3]
		filter_pass = call[6]

		detected_len = call[9].split(":")[2]

		# Check confidence intervals and if the result is not in the CI then write into FilterPass field BADCI
		if re.search(r'/', detected_len):
			detected_len_allele1 = int(detected_len.split("/")[0])
			detected_len_allele2 = int(detected_len.split("/")[1])

			CI_allele1 = CI.split("/")[0]
			CI_allele2 = CI.split("/")[1]
			CI_allele1_low = int(CI_allele1.split("-")[0])
			CI_allele1_high = int(CI_allele1.split("-")[1])

			CI_allele2_low = int(CI_allele2.split("-")[0])
			CI_allele2_high = int(CI_allele2.split("-")[1])

			if detected_len_allele1 < CI_allele1_low or detected_len_allele1 > CI_allele1_high:
				filter_pass += ',' if filter_pass != "PASS" else ""
				filter_pass += 'BADCI_A1'

			if detected_len_allele2 < CI_allele2_low or detected_len_allele2 > CI_allele2_high:
				filter_pass += ',' if filter_pass != "PASS" else ""
				filter_pass += 'BADCI_A2'

		else:
			detected_len_allele1 = int(detected_len)
			detected_len_allele2 = 'NA'
			CI_allele1 = CI
			CI_allele1_low = int(CI_allele1.split("-")[0])
			CI_allele1_high = int(CI_allele1.split("-")[1])

			if detected_len_allele1 < CI_allele1_low or detected_len_allele1 > CI_allele1_high:
				filter_pass += ',' if filter_pass != "PASS" else ""
				filter_pass += 'BADCI'

		# Determine whether it is homozygous or heterozygous
		if detected_len_allele1 == detected_len_allele2:
			genotype = 'HOM'
		else:
			genotype = 'HET'

		consistent_reads_total = int(STR_coverage_spanning[0]) + int(STR_coverage_inrepeat[0]) + int(STR_coverage_flanking[0])
		consistent_reads_spanning = int(STR_coverage_spanning[0])

		call_info = [
			int(detected_len_allele1),
			int(detected_len_allele2),
			CI_allele1,
			CI_allele2,
			consistent_reads_total,
			consistent_reads_spanning,
			str(filter_pass)
		]
	else:
		call_info = [
			"NA", 
			"NA", 
			"NA", 
			"NA", 
			"NA", 
			"NA",
			str(call[6])
		]

	return(call_info)


# Get the results from repeated homozygous samples
def get_results_rep_expansionhunter(file):
	sample_file = input_eh_folder + disorder_name + "/" + file
	no_repeats = int(re.search(r'(\d*)rep.vcf', file).group(1))
	results = []

	with open(sample_file, mode="r") as openfile:
		content = csv.reader(openfile, delimiter="\t")

		for row in content:
			if row[0].startswith('#'): # Discard the lines that start with #
				continue

			gene = re.search(r"REPID=(.*)", row[7]).group(1).upper()

			# Only take the locus of our gene
			if gene == disorder_gene:
				results = get_call_eh(row)
				results = ["repeats", 0, 0, no_repeats, no_repeats] + results

	return results


# Get the results from disease samples
def get_results_dis_expansionhunter(file):
	sample_file = input_eh_folder + disorder_name + "/" + file

	# Get the simulated length of alleles
	sample = re.search(r'(.*).vcf', file).group(1)
	sample_allelesstat_file = input_eh_folder + "../fa/" + disorder_name + "/alleles.stat"

	stats = []
	with open(sample_allelesstat_file, mode="r") as openfile:
		content = csv.reader(openfile, delimiter="\t")
		for row in content:
			if row[0] == sample:
				sample_type = row[0]
				repeats = row[1].split(", ")
				if "(" in repeats[1]:
					rep_a1 = re.search(r'(.*)\((\d*)\)', repeats[0])
					rep_a1_normal = int(rep_a1.group(1))
					rep_a1_path = int(rep_a1.group(2))

					rep_a2 = re.search(r'(.*)\((\d*)\)', repeats[1])
					rep_a2_normal = int(rep_a2.group(1))
					rep_a2_path = int(rep_a2.group(2))

				else:
					rep_a1_normal = 0 # Zero because if we are not dealing with complex repeats then we always have only the pathogenic motif
					rep_a2_normal = 0
					rep_a1_path = int(repeats[0])
					rep_a2_path = int(repeats[1])

				stats.append([
							sample_type,
							rep_a1_normal,
							rep_a2_normal,
							rep_a1_path,
							rep_a2_path
				])

	results = []
	with open(sample_file, mode="r") as openfile:
		content = csv.reader(openfile, delimiter="\t")
		for row in content:
			if row[0].startswith('#'): # Discard the lines that start with #
				continue

			gene = re.search(r"REPID=(.*)", row[7]).group(1).upper()

			# Only take the locus of our gene
			if gene == disorder_gene:
				results = get_call_eh(row)
				results = stats[0] + stats[1:4] + results

	return results


def extract_results():
	# Write the header
	results_header = ["disorder", "gene", "sample_type", "sim_allele1_normal", "sim_allele2_normal", "sim_allele1_path", "sim_allele2_path", "detected_allele1", "detected_allele2", "ci_allele1", "ci_allele2", "consistent_reads_total", "consistent_reads_spanning", "filter"]
	with open(output_file, "w") as outfile:
		outfile.write("\t".join(str(item) for item in results_header) + "\n")

	# Select all files in a folder and exctract data
	results = []
	files_count = 0
	for file in os.listdir(input_files_folder):
		if file.endswith(".vcf"):
			files_count += 1
			if simulated_samples == "repeats":
				results.append(get_results_rep_expansionhunter(file))
			elif simulated_samples == "disease":
				results.append(get_results_dis_expansionhunter(file))

	results = sorted(results, key=lambda x: (x[3])) # Sort array based on the simulated allele 1 of the pathogenic repeat length

	write_results_file(results) # Save into the results file

	print("Completed! Results for " + disorder_name + " (gene " + disorder_gene + ") were extracted out from total of " + str(files_count) + " VCF files and saved into " + output_file)

	return True


# Write results into the results file
def write_results_file(results):
	with open(output_file, "a") as outfile:

		for res in results:
			outfile.write(disorder_name + "\t" + disorder_gene + "\t")
			outfile.write("\t".join(str(item) for item in res) + "\n")
	
	return True


def main():
	global disorder_name, disorder_gene, output_file, input_files_folder, input_eh_folder, simulated_samples
	args = parse_args()
	disorder_name = args.disorder
	disorder_gene = args.gene.upper()
	output_file = args.output
	input_eh_folder = args.input_eh_folder
	simulated_samples = args.simulated_samples
	input_files_folder = input_eh_folder + disorder_name + "/"

	# Create the folder for output files if it doesn't exist yet
	output_path = os.path.dirname(output_file)
	Path(output_path).mkdir(parents=True, exist_ok=True)

	if os.path.exists(input_files_folder) and os.path.isdir(input_files_folder):
		if not os.listdir(input_files_folder):
			print("Directory exists, but is empty, exiting...")
			exit()
		else:
			print("Processing files in the " + input_files_folder + " directory...")
			extract_results()

	else:
		print("Directory does not exist, exiting...")
		exit()


if __name__ == "__main__":
	main()
