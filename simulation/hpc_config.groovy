// EXPANSIONHUNTER = 'ExpansionHunter-v4.0.1'
EXPANSIONHUNTER = 'ExpansionHunter-v3.3.0-rc9'
EH_VARIANT_CATALOG = './variant_catalog_hg38.json'

REF_GENOME = 'hg38'
REF_FILE = '../reference/hg38/hg38.fa'
REF_FOLDER = '../reference/hg38/chroms/'

BWA_THREADS = 2
PLATFORM = 'illumina'
