#####################################################################################################################
# Simulates alleles in increasing number of repeats for each disorder                                               #
# Example usage: python3 simulate_hom_rep.py --loci loci.sequences --output_folder out_500rep/fa/ --min 2 --max 500 #
#####################################################################################################################

import csv
import random
from pathlib import Path
from argparse import (ArgumentParser, FileType)

# Arguments from command line
def parse_args():
	parser = ArgumentParser(description='Simulates alleles in increasing number of repeats for each disorder')
	parser.add_argument('--loci', type=str, required=True, help='File which includes information about disorders and flanking sequences')
	parser.add_argument('--min', type=str, required=True, help='Minimum length of repeats in base pairs')
	parser.add_argument('--max', type=str, required=True, help='Maximum length of repeats in base pairs')
	parser.add_argument('--flanking', type=str, default=True, required=False, help='Create flanking ends or not and create only repeats (for finding offtarget positions)?', choices=["True", "False"])
	parser.add_argument('--output_folder', type=str, required=True, help='Folder for output files')
	
	return parser.parse_args()


# Open file and get details about loci to simulate
def import_loci(file):
	global disorders
	disorders = []

	with open(file, mode="r") as openfile:
		content = csv.reader(openfile, delimiter="\t")

		for row in content:
			disorder = {
				"name": row[0],
				"gene": row[1],
				"chrom": row[2],
				"start_position": int(row[3]),
				"motif": row[4],
				"prefix": row[5],
				"suffix": row[6]
			}
			disorders.append(disorder)

	return disorders

# Replace all N values with randomly chosen nucleotide
def replace_nucleotides(sequence):

	# All symbols that can be used to replace a placeholder nucleotide for another one
	nucleotides = {
		"R": "GA",
		"Y": "CT",
		"K": "GT",
		"M": "AC",
		"S": "GC",
		"W": "AT",
		"B": "GTC",
		"D": "GAT",
		"H": "ACT",
		"V": "GCA",
		"N": "AGCT"
	}

	new_sequence = ""

	# Go through each nucleotide and replace the ones than can be with a randomly chosen nucleotide from the list above
	for base in sequence:
		new_sequence += base.replace(base, random.choice(nucleotides[base])) if base not in "AGCT" else base

	return new_sequence

# Generate fasta files for each repeat length
def generate_fastas(disorder):
	motiflen = len(disorder["motif"])
	range_min_repeats = int(range_min_bp / motiflen) if int(range_min_bp / motiflen) >= 2 else 2
	range_max_repeats = int(range_max_bp / motiflen)+1 if int(range_max_bp / motiflen) > range_min_repeats else range_min_repeats+1

	for i in range(range_min_repeats, range_max_repeats, 1):
		repeated_seq = disorder["motif"] * i
		seq_nucl_replaced = replace_nucleotides(repeated_seq) # If there are any N bases, then replace them randomly with one of the 4 nucleotides

		if create_flanking == "False":
			seq = seq_nucl_replaced
			seq_len = len(seq)
			start_position = disorder["start_position"]
			end_position = start_position + len(seq)
		else:
			seq = disorder["prefix"] + seq_nucl_replaced + disorder["suffix"]
			seq_len = len(seq)
			start_position = disorder["start_position"] - len(disorder["prefix"])
			end_position = start_position + len(seq)

		i_str = str(i)

		if len(i_str) == 1:
			i_filename = "00" + i_str
		elif len(i_str) == 2:
			i_filename = "0" + i_str
		else:
			i_filename = i_str

		output_path = output_files_folder + "fa/" + disorder["name"] + "/"
		Path(output_path).mkdir(parents=True, exist_ok=True)

		f = open(output_path + disorder["name"] + "_" + i_filename + "rep.fa", "w")
		f.write(">" + disorder["chrom"] + ":" + str(start_position) + "-" + str(end_position) + "\n")
		f.write(str(seq))
		f.close()


def main():
	global loci_file, create_flanking, range_min_bp, range_max_bp, output_files_folder
	args = parse_args()
	loci_file = args.loci
	create_flanking = args.flanking
	range_min_bp = int(args.min)
	range_max_bp = int(args.max) + 1
	output_files_folder = args.output_folder

	disorders = import_loci(loci_file)

	for disorder in disorders:
		print("Generating files for: " + disorder["name"])
		generate_fastas(disorder)


if __name__ == "__main__":
	main()
