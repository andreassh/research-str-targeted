# Creates variant catalog based on the disorders list and offtarget regions files
# Use: python3 create_variant_catalog.py --out ./ [--offtargets true] [--bams ./out_ot/bam/]

import re
import os
import csv
import time
import json
import pysam
import pprint
import operator
from argparse import (ArgumentParser, FileType)
from collections import defaultdict

config = {
	'threshold_reads': 5, # How many reads in a region is required to keep this (below that will be filtered out)
	'distance_to_merge': 100, # Distance between two regions to merge, in base pairs
	'ot_end_len': 100, # Base pairs to add before and after the off-target locus

	'disorders_file': "../reference/strs.loci",
	'output_catalog_file_name': "variant_catalog_hg38_" + time.strftime("%Y%m%d") + ".json"
}


def parse_args():
	parser = ArgumentParser(description='Find off-target regions and create a new variant catalog')
	parser.add_argument('--offtargets', type=str, required=False, default='', help='Include off-target regions?')
	parser.add_argument('--bams', type=str, required=False, help='Folder where folders each disease including simulated BAM file are placed')
	parser.add_argument('--out', type=str, required=True, help='Folder where the catalogue will be created')
	return parser.parse_args()


def merge_intervals(array):
	l = len(array)

	if l > 1:
		for i in range(1, l):
			array[i-1] = array[i-1] + [1] if len(array[i-1]) == 2 else array[i-1]

			check = False
			check = array[i-1] and abs(array[i][0] - array[i-1][0]) <= config['distance_to_merge']

			if not check and array[i-1] and abs(array[i][0] - array[i-1][0]) <= config['distance_to_merge'] or abs(array[i][1] - array[i-1][1]) <= config['distance_to_merge']:
				check = True

			elif not check and array[i-1] and abs(array[i][1] - array[i-1][0]) <= config['distance_to_merge']:
				array[i-1], array[i] = array[i], array[i-1]
				check = True
		
			if check:
				if array[i][1] > array[i-1][1]: # Update the region
					array[i] = [array[i-1][0], array[i][1], array[i-1][2]+1]

				else: # Leave the region same
					array[i] = array[i-1]
					array[i-1][2] = array[i-1][2]+1

				array[i-1] = None

			if i == l-1 and len(array[i]) == 2:
				array[i] = array[i] + [1]

		return [i for i in array if i] # Return number of regions received and the merged list of regions
	
	else:
		array = [[array[0][0], array[0][1], 1]]
		return array


def create_offtargets(bamfile, disorder):
	readarr = []
	contigs = []
	input_bam = pysam.AlignmentFile(bamfile, 'rb')

	for read in input_bam.fetch():
		if read.reference_start == None or read.reference_end == None:
			continue
		else:
			contigs.append(read.reference_name)
			readarr.append([read.reference_name, read.reference_start-config['ot_end_len'], read.reference_end+config['ot_end_len']]) # Add some base pairs in the ends of the locus, seems to be important for EH to use these regions

	contigs = sorted(set(contigs))

	offtargets = []
	for contig in contigs:
		if "_" in contig: # Do not include alternative contigs
			continue

		read_positions = []
		catalogueitem = {}

		for region in readarr:
			if region[0] == contig:
				read_positions.append([region[1], region[2]])

		read_positions.sort()
		list_merged = merge_intervals(read_positions)

		for item in list_merged:
			start, end, reads = item
			coord = str(contig) + ":" + str(start) + "-" + str(end)

			# If less than X reads are on that position then exclude
			if reads <= config['threshold_reads']:
				continue

			# If the STR locus is in the reference region (+/- 50 bp) them exclude
			if max(start, int(disorder["coord_str_start"])-50) <= min(end, int(disorder["coord_str_end"])+50):
				continue

			offtargets.append(coord)

	return offtargets


def read_disorders():
	disorders = []

	with open(config["disorders_file"], mode = "r") as infile:
		reader = csv.reader(infile, delimiter = "\t")
		headers = next(reader, None)

		for col in reader:
			coordinates = re.split(':|-', col[8])
			is_not_in_reference = True if col[2] == "XYLT1" else False

			disorder = {
				"name": col[0],
				"inheritance": col[1],
				"gene": col[2],
				"location": col[3],
				"motif": col[4],
				"coord_chrom": coordinates[0],
				"coord_str_start": int(coordinates[1]),
				"coord_str_end": int(coordinates[1])+1 if is_not_in_reference == True else int(coordinates[2]) # In case of nested or replaced type of repeats then create a 1 bp reference region because we don't know the exact start or end position of the pathogenic expansion
			}

			disorders.append(disorder)

	return disorders


def main():
	args = parse_args()
	folder = args.bams
	req_offtargets = args.offtargets
	req_output_file = args.out

	disorders = read_disorders()
	variants = []

	for disorder in disorders:
		offtargets = []

		if req_offtargets and "N" not in disorder["motif"]: # Do not create off-targets for GCNs
			bamfolder = os.path.join(folder, disorder["name"])
			for file in os.listdir(bamfolder):
				if file.startswith(disorder["name"] + "_") and file.endswith(".bam"):
					bamfile = os.path.join(bamfolder, file)
					offtargets = create_offtargets(bamfile, disorder)

		
		# If more than 1 sample will be used then need to merge BED regions, atm it works well on one sample
		disorder_coord = disorder["coord_chrom"] + ":" + str(disorder["coord_str_start"]) + "-" + str(disorder["coord_str_end"])
		variant = {
			"LocusId": disorder["gene"],
			"LocusStructure": "(" + disorder["motif"] + ")*",
			"ReferenceRegion": disorder_coord,
			"VariantType": "RareRepeat" if offtargets else "Repeat"
			}

		if offtargets and "N" not in disorder["motif"]: # Only generate offtargets when the motif is not variable (i.e. doesn't include "N")
			variant.update({
				"OfftargetRegions": offtargets
			})
		
		variants.append(variant)

		variants.sort(key=operator.itemgetter('LocusId'))


	with open(req_output_file + config["output_catalog_file_name"], 'w') as outfile:
		json.dump(variants, outfile, indent=4)

	print("Finished")

if __name__ == "__main__":
	main()
