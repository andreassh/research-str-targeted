#!/usr/bin/env bash

output_folder=../out_ot/
repeats_min_bp=900 # In base pairs. NB! Minimum number of repeats is always 2 even when specified less base pairs in here
repeats_max_bp=900 # In base pairs

reference_fasta=../../reference/hg38/hg38.fa
reference_chroms_folder=../../reference/hg38/chroms/
reference_genome=hg38
disorders_list=../../reference/strs.loci
loci_sequences_list=../../reference/loci.sequences


echo "Do you wish to run the program on all files now?"
select sim in "Yes" "No"; do
	case $sim in
		Yes )
			mkdir -p ${output_folder} ${output_folder}/offtargets

			module load bpipe

#			Next line commented out as this should be done once and then manually edit the file to add non-pathogenic motifs to flanking sequences for nested type of repeats:
#			python3 ../simulate_dis_samples.py --reference ${reference_fasta} --reference_folder ${reference_chroms_folder} --genome ${reference_genome} --disorders ${disorders_list} --output_folder ../../reference/ --only_loci true

			python3 ../simulate_hom_rep.py --loci ${loci_sequences_list} --output_folder ${output_folder} --min ${repeats_min_bp} --max ${repeats_max_bp}

			while read -a line;
			do
				./hpc_pipeline_findofftargets.sh ${line[0]} ${output_folder}

			done < ${loci_sequences_list}; 

			python3 ../create_variant_catalog.py --offtargets true --offtargets_folder ${output_folder}/offtargets/ --out ${output_folder}/

			break;;

		No ) exit;;
	esac
done

echo "Reached to the end of the list!"
