sim_bt = {
	produce(output.prefix.prefix + '.offtargets') {
		exec """
        	        bedtools merge -nobuf -d 100 -i $input.bam > $output.prefix.prefix".offtargets"
        	""", "superquick"
	}
}

run {
     	~'(.*).bam' * [
                sim_bt
        ]
}
