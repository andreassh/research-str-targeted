load "../hpc_config.groovy"

def get_filename(path) {
	x = path.split("/")[-1]
	return(x)
}
def get_fileinfo(filename) {
	return(filename.split('/')[-1].split('\\.'))
}

// Simulate reads based on the simulated reference files, make homozygous alleles when simulating HiSeqX PCR free (150bp) reads with fragment length of 450 bp and quality scores reduced by 10 to simulate 1% error OR reduced by 20 to simulate 10% error
sim_reads = {
	def fa_filename = get_filename(input.fa)
	preserve("*.fq") {
		from("*.fa") {
			produce(fa_filename.prefix.prefix + ".R1.fq", fa_filename.prefix.prefix + ".R2.fq") {
				exec """
					art_illumina \
					-q \
					-ss HSXn \
					-p \
					-na \
					-l 150 \
					-f 100 \
					-m 450 \
					-s 50 \
					-qs -10 \
					-qs2 -10 \
					-i $input.fa \
					-o $output1.prefix.prefix".R"
				""", "superquick"
			}
		}
	}
}

// Align reads on the reference genome
sim_align = {
	def fileinfo = get_fileinfo(inputs)
	def samplename = fileinfo[0]
	produce(samplename + '.bam') {
		exec """
				bwa mem -M -t $BWA_THREADS \
				-R "@RG\\tID:${samplename}\\tPL:$PLATFORM\\tPU:1\\tLB:${samplename}\\tSM:${samplename}" \
				$REF_FILE $inputs | \
				samtools view -bSuh - | samtools sort -o $output.bam -
		""", "superquick"
	}
}

// Create index for BAM
sim_indxbam = {
	exec """
		samtools index $input.bam
	""", "superquick"
	forward input.bam
}

run {
	~'(.*).fa' * [ sim_reads ] + 
		~'(.*).R*.fq' * [ sim_align + sim_indxbam ]
}
