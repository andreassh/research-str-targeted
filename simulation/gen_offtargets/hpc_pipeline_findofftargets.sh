#!/usr/bin/env bash

# Input disease is $1 and output folder is $2

if [[ -z $1 ]]; then
	echo "A disorder was not supplied"
else
	echo "Running pipeline for $1"

	mkdir -p $2/fq/$1 $2/bam/$1 $2/offtargets

	cd $2/fq/$1/
	bpipe run ../../../gen_offtargets/hpc_fa2bam_findofftargets.groovy ../../fa/$1/*.fa
	mv *.bam* ../../bam/$1/

	cd ../../bam/$1/
	bpipe run ../../../gen_offtargets/hpc_bam2bed.groovy ../../bam/$1/*.bam

	mv *.offtargets ../../offtargets/

	echo "Pipeline finished for $1"
fi
