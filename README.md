Validating disease-associated STRs loci by using [ExpansionHunter](https://github.com/Illumina/ExpansionHunter) with simulations (see the [simulation](simulation/) folder).
